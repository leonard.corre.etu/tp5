import Page from './Page';
import PizzaThumbnail from '../components/PizzaThumbnail';

export default class PizzaList extends Page {
	#pizzas;

	constructor(pizzas) {
		super('pizzaList'); // on pase juste la classe CSS souhaitée
		this.pizzas = pizzas;
	}

	mount(element) {
		super.mount(element);
		// appel ajax lorsque l'on affiche la page
		fetch('http://localhost:8080/api/v1/pizzas')
			.then(response => response.json())
			.then(data => (this.pizzas = data)) //  maj des children
			.then(() => (this.element.innerHTML = this.render())); // affichage dans la page
	}

	set pizzas(value) {
		this.#pizzas = value;
		this.children = this.#pizzas.map(pizza => new PizzaThumbnail(pizza));
	}
}
